## Process

Step 1: Building search engine for images using Approximate Nearest Neighbours(ANN) algorithms using  ANNOY/HNSWLib

Step 2: Create a Flask application to read the input images and display the nearest results

Step 3: Push the images and model artifacts to S3 Bucket

Step 4: Write the logic to read the images from S3 Buckets based on key values from the model artifacts

Step 5: Create the Docker Image of the Flask application

Step 6: Push the Docker image to Docker Hub or AWS ECR repository

Step 7: Push the code to Gitlab and register and start the Gitlab CICD runners

Step 8: Create a CICD pipeline in gitlab
        Write the commands 
        to :
        1. create a docker image and push to Docker hub/AWS ECR 
        2. Create a Fargate Cluster in ECS
        3. Create a task in ECS tasks
        4. Run the created task in the ECS cluster

Step 9: Use the public IP to run the application


### Generating feature vectors from images
The feature vector of an image can be described as a dense vector representation of the image that can be obtained after running a sequence of operations, such as applying kernels, pooling, etc. This dense vector representation can be used for a variety of tasks such as classification, clustering, ranking and more.
![](image-files/Extracting_Features.png)

### Approximate Nearest Neighbours(ANN) algorithms

Algorithm for finding points in space that are near a given query point, such as a specific point of
interest. It also produces massive data structures that are mapped into memory, allowing several
processes to share data. The nearest neighbors issue is a similarity problem that seeks for the points
that are most similar to a given instance.Annoy is an empirically developed algorithm that has been
regarded as one of the greatest ANN libraries and has been utilized in the recommendation engine on
Spotify.com.

![](image-files/Annoy.png)

### HNSW-Lib

The HNSW graph algorithm is a fast and accurate solution to the approximate k-nearest neighbors
(k-NN) search problem.A straightforward, yet naive solution to the k-NN problem is to first compute the distances from a
given query point to every data point within an index and then select the data points with the smallest
k distances to the query. While this approach is effective when the index contains ten thousand or
fewer data points, it does not scale to the sizes of datasets used by our customers. An approximate
k-NN (ANN) algorithm may greatly reduce search latency at the cost of precision. When designing
an ANN algorithm there are two general approaches to improve latency - compute fewer distances
and make distance computations cheaper

![](image-files/HNSW.png)
