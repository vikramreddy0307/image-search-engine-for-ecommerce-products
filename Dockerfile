# FROM ubuntu
FROM ubuntu
# RUN apk add --update && apk add libglib2.0-0 libgl1-mesa-glx && rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get install -y libglib2.0-0 libgl1-mesa-glx && rm -rf /var/lib/apt/lists/*

# RUN apt-get update && apt-get install -y python3-opencv
RUN apt-get update -y && apt-get install -y python3.10 python3-pip python3.10-dev 



COPY ./requirements.txt /image-search/requirements.txt

WORKDIR /image-search

RUN pip install --upgrade pip


RUN pip install -r requirements.txt

COPY . /image-search

ENTRYPOINT ["python3"]

CMD ["server_s3.py"]
