import os
from flask import *
import datetime
from werkzeug.utils import secure_filename
import tensorflow as tf
import tensorflow_hub as hub
import pathlib
import pickle
from urllib.parse import urljoin
import boto3
import cv2
import numpy as np
'''
pipreqs  ..\web_app\
'''

app = Flask(__name__)

timestamp = datetime.datetime.now()

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
'''
Reading files from AWS S3 bucket
'''
s3 = boto3.client('s3')
s3 = boto3.resource(
    service_name='s3',
    region_name='us-east-2',
    aws_access_key_id='AKIAW2C7OCPGVWKDEGUA',
    aws_secret_access_key='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'
)
# Print out bucket names
for bucket in s3.buckets.all():
    print(bucket.name)

# Load pickle file directly into python
hnsw = s3.Bucket(bucket.name).Object('hnsw_efficientnet_index_cosine.pickle').get()
efficientnet_vectors = s3.Bucket(bucket.name).Object('efficientnet_feature_vectors.pkl').get()
train_file=s3.Bucket(bucket.name).Object('train.pickle').get()
db_file=s3.Bucket(bucket.name).Object('db.pickle').get()

S3_IMAGES_PATH = 'https://image-search-engine-model-files.s3.amazonaws.com/images/'

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}
hnsw_index = pickle.loads(hnsw['Body'].read())

# pickle.loads(my_bucket.Object("hnsw_efficientnet_index_cosine.pickle").get()['Body'].read())
efficentnet_fvs = pickle.loads(efficientnet_vectors['Body'].read())

train = pickle.loads(train_file['Body'].read())
db = pickle.loads(db_file['Body'].read())


print('TF version:', tf.__version__)
print('Hub version:', hub.__version__)
print('Phsical devices:', tf.config.list_physical_devices())

tf.keras.backend.clear_session()
print("Building model...")
# m = hub.KerasLayer('https://tfhub.dev/google/imagenet/efficientnet_v2_imagenet21k_l/feature_vector/2', trainable=False)
# m.build([None, 480, 480, 3])  # Batch input shape.
# m = hub.KerasLayer('https://tfhub.dev/google/imagenet/efficientnet_v2_imagenet21k_s/feature_vector/2', trainable=False)
# m.build([None, 384, 384, 3])  # Batch input shape.
m = hub.KerasLayer('https://tfhub.dev/google/imagenet/efficientnet_v2_imagenet1k_b3/feature_vector/2', trainable=False)
m.build([None, 300, 300, 3])  # Batch input shape.
print("Model built.")


print("Generating image feature vectors...")
# for jpeg_file in tqdm.tqdm(list(path_to_jpegs.iterdir())):
def process_image(image):
    # image_file = jpeg_file
    # image = tf.keras.preprocessing.image.load_img(image_file, target_size=(300, 300))
    # image = tf.keras.preprocessing.image.img_to_array(image)
    image = (image - 128.) / 128.
    logits = m(tf.expand_dims(image, 0), False)
    # feature_vectors[jpeg_file.stem] = logits.numpy()[0]
    return logits.numpy()[0]


def nearest_neighbours(filepath):
    logits = process_image(filepath)
    nearest = hnsw_index.knn_query(logits, k=100)[0][0]
    print(nearest)
    visited = set()
    for nn in nearest:
        nn_product_id = train[nn].split('_')[0]
        if nn_product_id not in visited and len(visited) < 20:
            visited.add(nn_product_id)
    
    
    product_data = []
    for product_id in list(visited):
        data = db[product_id]
        data['id']=urljoin(S3_IMAGES_PATH,data['id']+'_0.jpg')
        product_data.append(data)
        
        print("product_data", data['id'])
    return product_data


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS
           
@app.route('/')
def index():
    return redirect('/submit')
    # return render_template('index.html')

@app.route('/submit', methods=['POST', 'GET'])
def submit():
    if request.method == 'POST':
        file = request.files['query_image']
        if file and allowed_file(file.filename):
            # convert string of image data to uint8
            nparr = np.fromstring(file.read(), np.uint8)
            # decode image
            img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
            # filename = secure_filename(file.filename)
            # filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            # file.save(filepath)
            product_data = nearest_neighbours(img)
            product_data.reverse()
            return render_template('index.html', product_data=product_data)
        else:
            return 'Invalid file'
                
    return render_template('submit2.html')


# @app.route('/cdn/<filename>')
# def downloads(filename):

#                                filename)

if __name__ == "__main__":
    app.run( host='0.0.0.0')

#docker run -p 8000:8000 --bind 0.0.0.0