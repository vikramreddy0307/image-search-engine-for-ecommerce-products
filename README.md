# Continuous Integration and Conitnuous Developement of Image Search Engine for E-Commerce Products

In this project, I have built a reverse image-based search engine for fashion products that aims to gather similar images based on an input example image given by the user. I used CNNs for studying feature representation on image data. 

Then, these trained CNN models such as Inception V3 , Resnet50 , and EffecientNet V2 B3  are used to perform a nearest-neighbor analysis using KD-Tree ,Annoy, and HSNW  to return the most similar and relevant images to the input image.

Finally, I also created a small web application where a user can upload an image of their own and get visually similar products from e-commerce websites and deployed the application in AWS. 

Visit the page for step by step process [process.md](https://gitlab.com/vikramreddy0307/image-search-engine-for-ecommerce-products/-/blob/main/process.md)

### Screenshots of CICD process and application running on AWS ECS fargate cluster
![](image-files/CICD.png)
![](image-files/Cluster.png)
![](image-files/Home-page.png)
![](image-files/similar-products.png)


### Topics I have learnt during this process

1. Docker and AWS Networking (IP's, Ports, VPC, Subnets, Security groups, AWS Services (ECS, ECR, S3 Bucket))
2. Creating abd Building Docker images
3. Building and Running CICD pipelines in gitlab
4. AWS CLI scripts for the CICD process and deploying the application in AWS


### Helpful resources:

1. AWS Networking : 
        https://youtu.be/rg2BTWVCQlY
        https://youtu.be/cEbJZdZxJ5A
2. Docker Networking
        https://youtu.be/6by0pCRQdsI
3. Gitlab CICD pipelines
        https://youtu.be/qP8kir2GUgo
4. Deploying an application in AWS
        https://youtu.be/YDNSItBN15w
